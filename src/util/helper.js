const shuffle = str => {
  return [...str].sort(() => Math.random() - 0.5).join('');
};

export {
    shuffle
}