import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import dashboard from '../screens/dashboard';
import gameScreen from '../screens/gameScreen';
import success from '../screens/success';
import learship from '../screens/learship';

const Stack = createNativeStackNavigator();

const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Dashboard"
          component={dashboard}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="GameScreen"
          component={gameScreen}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Success"
          component={success}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="learship"
          component={learship}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MyStack;
