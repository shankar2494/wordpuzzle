import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import React, {useState} from 'react';
import BootSplash from 'react-native-bootsplash';
import {ScaledSheet} from 'react-native-size-matters'; // Fou UI responsiveness
import FastImage from 'react-native-fast-image';
import Images from '../components/Images';
import {buttonName, leadershipBoard, title} from '../components/Strings';

const dashboard = ({navigation}) => {
  const [puzzles, setPuzzles] = React.useState([
    {
      id: 1,
      name: 'Animals',
      selected: false,
      question: 'Which animal is known as the king of the jungle?',
      answer: 'LION',
      letterCount: 4,
    },
    {
      id: 2,
      name: 'Countries',
      selected: false,
      question: 'Which country is known as the birthplace of Pizza?',
      answer: 'CANADA',
      letterCount: 6,
    },
    {
      id: 3,
      name: 'Fruits',
      selected: false,
      question: 'Where was the kiwi fruit first grown?',
      answer: 'CHINA',
      letterCount: 5,
    },
  ]);

  const [challengeName, setChallengeName] = React.useState('');
  const [transferData, setTransferData] = useState({});

  React.useEffect(() => {
    BootSplash.hide({fade: true}).then(() => {});
  });

  const filteredData = option => {
    puzzles.filter(item => {
      if (item.name === option.name) {
        item.selected = true;
        setChallengeName(item.name);
      } else {
        item.selected = false;
      }
    });
    setPuzzles([...puzzles]);
    setTransferData(option);
  };

  const button = ({item, index}) => {
    return (
      <TouchableOpacity
        style={[
          styles().buttonView,
          {backgroundColor: item.selected ? '#03e67c' : '#ffffff'},
        ]}
        onPress={() => filteredData(item)}>
        <Text
          style={[
            styles().buttonText,
            {color: item.selected ? '#ffffff' : '#000000'},
          ]}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles().container}>
      <View style={styles().topView}>
        <FastImage
          source={Images.appLogo}
          style={styles().logo}
          resizeMode={'contain'}
        />
        <Text style={styles().title}>{title}</Text>
      </View>
      <View style={styles().middleView}>
        <FlatList
          data={puzzles}
          renderItem={button}
          keyExtractor={item => item.id}
          contentContainerStyle={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        />
      </View>
      <View style={styles().bottomView}>
        <TouchableOpacity
          style={styles().buttonView1}
          onPress={() => {
            navigation.navigate('GameScreen', {
              gameName: challengeName,
              rawData: transferData,
            });
          }}
          disabled={!puzzles.some(item => item.selected === true)}>
          <Text style={[styles().buttonText, {color: '#ffffff'}]}>
            {buttonName}
          </Text>
        </TouchableOpacity>
        <Text
          style={styles().scoreLink}
          onPress={() => navigation.navigate('learship')}>
          {leadershipBoard}
        </Text>
      </View>
    </SafeAreaView>
  );
};

const styles = () =>
  ScaledSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ffffff',
    },
    logo: {
      height: '43@vs',
      width: '43@vs',
    },
    topView: {
      flex: 0.2,
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: '30@msr',
      marginTop: '10@vs',
      fontWeight: 'bold',
      color: '#000000',
    },
    middleView: {
      flex: 0.55,
      alignItems: 'center',
    },
    buttonView: {
      height: '50@vs',
      width: '180@vs',
      backgroundColor: '#ffffff',
      borderWidth: '3@vs',
      borderColor: '#03e67c',
      borderRadius: '5@vs',
      alignItems: 'center',
      justifyContent: 'center',
      marginVertical: '20@vs',
    },
    buttonText: {
      fontSize: '20@msr',
      fontWeight: 'bold',
      textAlign: 'center',
    },
    buttonView1: {
      height: '50@vs',
      width: '180@vs',
      backgroundColor: '#03e67c',
      borderWidth: '3@vs',
      borderColor: '#03e67c',
      borderRadius: '5@vs',
      alignItems: 'center',
      justifyContent: 'center',
    },
    bottomView: {
      flex: 0.25,
      alignItems: 'center',
      justifyContent: 'center',
    },
    scoreLink: {
      fontSize: '15@msr',
      color: '#3C68FF',
      marginTop: '15@vs',
      textDecorationLine: 'underline',
      fontWeight: '700',
    },
  });
export default dashboard;
