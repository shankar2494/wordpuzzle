import {
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Alert,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {ScaledSheet} from 'react-native-size-matters'; // Fou UI responsiveness
import {CodeField} from 'react-native-confirmation-code-field';
import {shuffle} from '../util/helper';
import {buttonName, next} from '../components/Strings';

const {width} = Dimensions.get('window');

const gameScreen = ({navigation,route}) => {
  const {gameName, rawData} = route.params;

  const [value, setValue] = useState([]);
  const [shuffledData, setShuffledData] = useState([]);
  const [error, setError] = useState(false);

  useEffect(() => {
    let receivedData = shuffle(rawData.answer).split('');
    setShuffledData(receivedData);
    return () => {};
  }, []);

  const tapValue = (item, index) => {
    setError(false);
    if (item != '') {
      shuffledData.splice(index, 1, '');
      setShuffledData([...shuffledData]);
      setValue([...value, item]);
    }
  };

  const buttonClick = () => {
    if (rawData.answer == value.join('')) {
      setError(false);
      navigation.navigate('Success')
    } else {
      setError(true);
    }
  };

  const clear = () => {
    let receivedData = shuffle(rawData.answer).split('');
    setShuffledData(receivedData);
    setValue([]);
    setError(false);
  };

  return (
    <SafeAreaView style={styles().container}>
      <View style={styles().topView}>
        <Text style={styles().heading}>{gameName}</Text>
      </View>
      <View style={styles().middleView}>
        <CodeField
          value={value.join('')}
          editable={false}
          onChangeText={value => {
            if (value.length === rawData.letterCount) {
              console.log(value);
            }
          }}
          rootStyle={styles().codeFieldRoot}
          keyboardType="default"
          cellCount={rawData.letterCount}
          renderCell={({index, symbol, isFocused}) => (
            <Text key={index} style={styles().cell}>
              {symbol}
            </Text>
          )}
        />
        <Text style={styles().question}>{rawData.question}</Text>
        <View style={styles().mapView}>
          {shuffledData.map((item, index) => {
            return (
              <TouchableOpacity
                style={styles().shuffledList}
                onPress={() => tapValue(item, index)}>
                <Text style={styles().shuffled}>{item}</Text>
              </TouchableOpacity>
            );
          })}
        </View>
        {error ? <Text style={styles().wrong}>{'Wrong'}</Text> : null}
      </View>
      <View style={styles().bottomView}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity style={styles().buttonView1} onPress={clear}>
            <Text style={styles().buttonText}>{'Clear'}</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles().buttonView1} onPress={buttonClick}>
            <Text style={styles().buttonText}>{next}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default gameScreen;

const styles = () =>
  ScaledSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#ffffff',
      paddingHorizontal: '10@vs',
    },
    topView: {
      flex: 0.2,
      alignItems: 'center',
      justifyContent: 'center',
    },
    middleView: {
      flex: 0.55,
    },
    bottomView: {
      flex: 0.25,
      alignItems: 'center',
      justifyContent: 'center',
    },
    heading: {
      fontSize: '30@msr',
      fontWeight: 'bold',
      color: '#000000',
    },
    root: {padding: 20, minHeight: 300},
    title: {textAlign: 'center', fontSize: 30},
    codeFieldRoot: {marginTop: 20, width: width - 50, alignSelf: 'center'},
    cell: {
      width: 50,
      height: 50,
      lineHeight: 38,
      fontSize: 24,
      borderWidth: 3.5,
      borderRadius: 5,
      borderColor: '#03e67c',
      textAlign: 'center',
      color: '#03e67c',
      paddingTop: 5,
      fontWeight: '700',
    },
    focusCell: {
      borderColor: '#000',
    },
    question: {
      fontSize: '17@msr',
      color: '#5F5F5F',
      marginTop: '20@vs',
      textAlign: 'center',
    },
    shuffled: {
      fontSize: '17@msr',
      color: '#5F5F5F',
    },
    shuffledList: {
      width: 50,
      height: 50,
      fontSize: 24,
      borderWidth: 3.5,
      borderRadius: 5,
      borderColor: '#03e67c',
      alignItems: 'center',
      justifyContent: 'center',
      marginRight: '5@vs',
    },
    mapView: {
      flexDirection: 'row',
      justifyContent: 'center',
      marginTop: '50@vs',
    },
    buttonView1: {
      height: '50@vs',
      width: '130@vs',
      backgroundColor: '#03e67c',
      borderWidth: '3@vs',
      borderColor: '#03e67c',
      borderRadius: '5@vs',
      alignItems: 'center',
      justifyContent: 'center',
      marginRight: '10@vs',
    },
    buttonText: {
      fontSize: '20@msr',
      fontWeight: 'bold',
      textAlign: 'center',
      color: '#ffffff',
    },
    wrong: {
      fontSize: '20@msr',
      fontWeight: 'bold',
      textAlign: 'center',
      color: 'red',
      marginTop: '30@vs',
    },
  });
