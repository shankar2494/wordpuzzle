import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {ScaledSheet} from 'react-native-size-matters'; // Fou UI responsiveness
import { next } from '../components/Strings';

const success = ({navigation}) => {
  return (
    <SafeAreaView style={styles().container}>
      <Text style={styles().text}>
        {'Correct\nCongratulation..!\n\nYou earn 10 points'}
      </Text>
      <TouchableOpacity style={styles().buttonView1} onPress={() => {navigation.navigate('Dashboard')}}>
        <Text style={styles().buttonText}>{next}</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default success;

const styles = () => ScaledSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  text: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#3556FF',
    textAlign: 'center',
  },
  buttonView1: {
    height: '50@vs',
    width: '130@vs',
    backgroundColor: '#03e67c',
    borderWidth: '3@vs',
    borderColor: '#03e67c',
    borderRadius: '5@vs',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '100@vs',
  },
  buttonText: {
    fontSize: '20@msr',
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#ffffff',
  },
});
