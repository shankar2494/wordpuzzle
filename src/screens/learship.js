import {SafeAreaView, StyleSheet, Text, View, FlatList} from 'react-native';
import React, {useState} from 'react';

const learship = () => {
  const [topPosition, setTopPosition] = useState([
    {
      id: 1,
      name: 'Shankar',
      score: 10,
    },
    {
      id: 2,
      name: 'Mohan',
      score: 9,
    },
    {
      id: 3,
      name: 'Anand',
      score: 8,
    },
    {
      id: 4,
      name: 'Sathish',
      score: 7,
    },
  ]);

  const scoreData = ({item}) => {
    return (
      <View style={styles.renderContainer}>
        <Text style={styles.name}>{item.name}</Text>
        <Text style={styles.name}>{item.score}</Text>
      </View>
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.heading}>Score Board</Text>
      <FlatList data={topPosition} renderItem={scoreData} />
    </SafeAreaView>
  );
};

export default learship;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  heading: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#03e67c',
    marginTop: 10,
  },
  renderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    marginTop: 20,
  },
  name: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#787878',
  },
});
