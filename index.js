/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import dashboard from './src/screens/dashboard';
import MyStack from './src/Router/navigationScreens';

AppRegistry.registerComponent(appName, () => MyStack);
